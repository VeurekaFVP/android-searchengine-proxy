package veureka.pt.services.proxy.searchengine;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;

import io.reactivex.Single;
import io.reactivex.functions.Function;
import veureka.pt.services.AException;
import veureka.pt.services.proxy.retrofit.RetrofitProxy;

public class SearchEngineProxy {
    public final static String MESSAGE_TYPE = "message";
    public final static String ENTITY_TYPE = "entity";
    private static SearchEngineProxy mInstance;

    private SearchEngineProxy() {

    }
    
    public static SearchEngineProxy createInstance(Context context, String username, String password, String baseURL) throws AException {
        for(ServiceRequestType serviceRequestType : ServiceRequestType.values()) {
            serviceRequestType.startEndPoint(context, username, password, baseURL);
        }
        if(mInstance == null) {
            mInstance = new SearchEngineProxy();
        }
        return mInstance;
    }
    
    public Single<SearchEngineModel> getRequest(Context context, String type, String index, JsonObject params) throws AException {
        Single<LinkedTreeMap<String, Object>> single;
        ServiceRequestType services = ServiceRequestType.SearchByType;
        single = RetrofitProxy.handleResponse(context, services, services.getServices(), params, index, type);
        return single.map(new Function<LinkedTreeMap<String, Object>, SearchEngineModel>() {
            @Override
            public SearchEngineModel apply(LinkedTreeMap<String, Object> stringObjectLinkedTreeMap) throws Exception {
                return new SearchEngineModel(stringObjectLinkedTreeMap);
            }
        });
    }

    public Single<JsonObject> getRawRequest(Context context, String type, String index, JsonObject params)  throws AException {
        Single<LinkedTreeMap<String, Object>> single;
        ServiceRequestType services = ServiceRequestType.SearchByType;
        single =  RetrofitProxy.handleResponse(context, services, services.getServices(), params, index, type);
        return single.map(new Function<LinkedTreeMap<String, Object>, JsonObject>() {
            @Override
            public JsonObject apply(LinkedTreeMap<String, Object> stringObjectLinkedTreeMap) throws Exception {
                Gson gson = new Gson();
                return gson.toJsonTree(stringObjectLinkedTreeMap).getAsJsonObject();
            }
        });
    }
}
