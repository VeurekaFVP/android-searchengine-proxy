package veureka.pt.services.proxy.searchengine;

import veureka.pt.services.proxy.retrofit.CredentialManager;

class SearchEngineCredential extends CredentialManager {
    
    private static SearchEngineCredential credentials;
    
    private SearchEngineCredential(String username, String password, String baseURL) {
        super(username, password, baseURL);
    }
    
    
    static SearchEngineCredential createInstance(String username, String password, String baseURL) {
        if(credentials == null) {
            credentials = new SearchEngineCredential(username, password, baseURL);
        } else {
            credentials.setBaseURL(baseURL);
            credentials.setPassword(password);
            credentials.setUsername(username);
        }
        return credentials;
    }
    
    //*
    public static SearchEngineCredential getInstance() {
        return credentials;
    }
    // */
    @Override
    public void setUsername(String username) {
        this.username = username;
    }
    
    @Override
    public void setPassword(String password) {
        this.password = password;
    }
    
    @Override
    public void setBaseURL(String baseURL) {
        this.baseURL = baseURL;
    }
    
    
    
}
