package veureka.pt.services.proxy.searchengine;

import android.content.Context;

import veureka.pt.services.AException;
import veureka.pt.services.proxy.retrofit.IServiceRequestType;

enum ServiceRequestType implements IServiceRequestType<SearchEngineProxyEndPoint> {
    SearchByEntity(SearchEngineEndPoints.SearchEngineProxyEndPoint, "searchByEntity"),
    SearchByType(SearchEngineEndPoints.SearchEngineProxyEndPoint, "searchByType");

    private String service;
    private SearchEngineEndPoints endPoints;
    
    ServiceRequestType(SearchEngineEndPoints endPoints, String service) {
        this.service = service;
        this.endPoints = endPoints;
    }
    
    @Override
    public String getService() {
        return this.service;
    }
    
    @Override
    public SearchEngineProxyEndPoint getServices() {
        return endPoints.getServices();
    }
    
    @Override
    public SearchEngineProxyEndPoint startEndPoint(Context context, String username, String password, String baseURL)
            throws AException {
        return endPoints.startEndPoint(context, username, password, baseURL);
    }
    
    @Override
    @Deprecated
    public SearchEngineProxyEndPoint startEndPoint(Context context, String token, String baseURL) throws AException {
        return endPoints.startEndPoint(context, token, baseURL);
    }
    
}
