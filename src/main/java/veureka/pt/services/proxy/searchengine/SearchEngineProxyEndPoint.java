package veureka.pt.services.proxy.searchengine;

import com.google.gson.internal.LinkedTreeMap;

import io.reactivex.Single;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

interface SearchEngineProxyEndPoint {
    
    @POST("{index}/entity/_search/template")
    Single<LinkedTreeMap<String, Object>> searchByEntity(@Path("index") String index, @Body RequestBody params);

    @POST("{index}/{type}/_search/template")
    Single<LinkedTreeMap<String, Object>> searchByType(@Path("index") String index, @Path("type") String type, @Body RequestBody params);

}
