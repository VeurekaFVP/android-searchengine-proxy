package veureka.pt.services.proxy.searchengine;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;

import java.util.ArrayList;

import io.reactivex.Observable;

public class SearchEngineModel {
    
    private ArrayList<JsonObject> hits;
    private int total;
    private JsonObject aggregations;
    
    @SuppressWarnings("unchecked")
    SearchEngineModel(LinkedTreeMap<String, Object> map) {
        hits = new ArrayList<>();
        total = 0;
        aggregations = null;
        
        if(map != null) {
            Gson gson = new GsonBuilder().create();
            JsonElement aggregationsRef = gson.toJsonTree(map.get("aggregations"));
            
            if(!aggregationsRef.isJsonNull()) {
                aggregations = aggregationsRef.getAsJsonObject();
            }
            
            LinkedTreeMap<String, Object> mainHits = (LinkedTreeMap<String, Object>) map.get(
                    "hits");
            if(mainHits != null) {
                ArrayList<Object> resultHits = (ArrayList<Object>) mainHits.get("hits");
                
                if(resultHits != null) {
                    gson = new GsonBuilder().create();
                    for(Object result : resultHits) {
                        hits.add(gson.toJsonTree(result).getAsJsonObject());
                    }
                }
                
                total = ((Double) mainHits.get("total")).intValue();
            }
        }
    }
    
    public ArrayList<JsonObject> getHits() {
        return hits;
    }
    
    public int getTotal() {
        return total;
    }
    
    public JsonObject getAggregations() {
        return aggregations;
    }
    
    public Observable<JsonObject> getHitsObservable() {
        return Observable.fromIterable(hits);
    }
}
