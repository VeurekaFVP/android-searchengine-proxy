package veureka.pt.services.proxy.searchengine;

import android.content.Context;

import veureka.pt.services.AException;
import veureka.pt.services.proxy.retrofit.CredentialManager;
import veureka.pt.services.proxy.retrofit.IServicesFactory;
import veureka.pt.services.proxy.retrofit.RetrofitProxy;

enum SearchEngineEndPoints implements IServicesFactory<SearchEngineProxyEndPoint> {
    SearchEngineProxyEndPoint(SearchEngineProxyEndPoint.class);

    private SearchEngineProxyEndPoint endPoint;
    private Class<SearchEngineProxyEndPoint> serviceClass;
    
    SearchEngineEndPoints(Class<SearchEngineProxyEndPoint> clazz) {
        this.serviceClass = clazz;
    }
    
    @Override
    public SearchEngineProxyEndPoint getServices() {
        return this.endPoint;
    }
    
    @Override
    public Class<SearchEngineProxyEndPoint> getServiceClass() {
        return this.serviceClass;
    }
    
    @Override
    public SearchEngineProxyEndPoint startEndPoint(Context context, String username, String password, String baseURL)
            throws AException {
        SearchEngineCredential.createInstance(username, password, baseURL);
        this.endPoint = RetrofitProxy.createEndPoint(context, this, true);
        return this.endPoint;
    }
    
    @Override
    @Deprecated
    public SearchEngineProxyEndPoint startEndPoint(Context context, String token, String baseURL)
            throws AException {
        
        if(token != null && !token.isEmpty()){
            SearchEngineCredential credentials = SearchEngineCredential
                    .createInstance(null, null, baseURL);
            credentials.setAuthToken(token);
            this.endPoint = RetrofitProxy.createEndPoint(context, this, true);
        }
        
        return this.endPoint;
    }
    
    
    @Override
    public CredentialManager getCredentials() {
        return SearchEngineCredential.getInstance();
    }
}
